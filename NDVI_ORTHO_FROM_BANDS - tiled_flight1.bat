@ECHO OFF
ECHO NDVI ORTHO FROM RGB, NIR 
ECHO RGB,NIR in separate folder "RGB" and "NIR" 
ECHO input TIFFs RGB AND NIR have to be the same with the same names
ECHO Author: Szczepkowski Marek
ECHO Date: 22-05-2019
ECHO Version: 1.0
ECHO QGIS 2.14

SET QGIS_ROOT=C:\Program Files\QGIS 2.14
REM Setup gdal_transalte.exe
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal
SET PYTHON=C:\Users\m.szczepkowski\AppData\Local\Programs\Python\Python37\Lib\site-packages\osgeo\scripts

REM Path to working dir
SET WORK=%cd%
REM Suffix output files
SET OUT_SUFFIX=_NDVI

FOR /F %%i IN ('dir /b "%WORK%\RGB\*.tiff"') DO (
    ECHO.
	ECHO Processing  %%i
	ECHO BUILD VRT FILES
	gdalbuildvrt "%WORK%\%%~ni_RGB_2.vrt" RGB\%%i
	gdalbuildvrt "%WORK%\%%~ni_NIR_2.vrt" NIR\%%i
	ECHO BUILD BANDS VRT FILES
	gdal_translate -of GTiff -b 1 -ot Float64 "%WORK%\%%~ni_RGB_2.vrt" "%WORK%\%%~ni_r_2.tif"
	gdal_translate -of GTiff -b 3 -ot Float64 "%WORK%\%%~ni_NIR_2.vrt" "%WORK%\%%~ni_nir.tif" 

	ECHO BUILD NDVI
	py -3.7 "%PYTHON%\gdal_calc.py" -A "%WORK%\%%~ni_r_2.tif" -B "%WORK%\%%~ni_nir.tif"  --calc="((B-A)/(B+A))" --type=Float64 --outfile="%WORK%\%%~ni%OUT_SUFFIX%.tif"
	DEL /Q "%WORK%\*.vrt"
)
ECHO.
ECHO Done
PAUSE